require('dotenv').config()
const express = require('express');
const cors = require('cors');

const authRouter = require('./routes/auth');
const postsRouter = require('./routes/posts');


const app = express();

// app.use(cors({origin: true, credentials: true}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", '*');
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/api/auth', authRouter);
app.use('/api/posts', postsRouter);

const port = process.env.PORT || 3030;

app.listen(port, () => console.log(`Listening to port ${port}`));
