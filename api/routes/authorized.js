// require('dotenv').config()
const jwt = require("jsonwebtoken");
//const config = require('../../config');
const jwtSecret = process.env.JWT_SECRET;

async function userAuthed(request) {
    //console.log(request.headers, typeof request.headers)
	const authHeader = request.headers['authorization']
	const token = authHeader && authHeader.split(' ')[1];
    //console.log(token)
    let userPack;
	if(!token) {
		//console.log(" no authorization header, or, not in proper format case (respectively) (this message was sent by De Morgan Gang");
		return [false, 400, "joe"];
	} else {
		// case where authorization token present in header in 'Bearer <token>' format

        try {
		    userPack = await jwt.verify(token, jwtSecret);
        } catch(e) {
            //console.log("jwt verify failed with the following error: ");
            //console.log(e);
            // jwt has been tampered with case
            return [false, 401, "joe"];
        }

		// to check if token was issued before a month (month seconds = 2.628e6)
		const currentUTCSec = Math.trunc((new Date()).getTime()/1000);
        const timeDiff = currentUTCSec - userPack.iat;
        //console.log("time diff is: ", timeDiff/60, "minutes")
		if( timeDiff > 2.628e6) {
            //console.log("old jwt, ie, session timed out");
			return [false, 440, "joe"];
		} else {
            //console.log("passed all the tests");
			//console.log(userPack)
			return [true, 200, userPack.name];
		}
	}
}

module.exports = {userAuthed};
