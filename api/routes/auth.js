// require('dotenv').config()
const { loadCollection } = require('./loadCollection');
const { userAuthed } = require('./authorized');
const express = require("express");
const argon2 = require("argon2");
//const config = require('../../config');
const jwt = require("jsonwebtoken");
const router = express.Router();

jwtSecret = process.env.JWT_SECRET;

// This Script is for Authentication and creation of users
// for authorization, please check authorized.js


async function userValidate(body) {
	// validate the req.body fields if empty or wierd characters
	// also, check if username already exists
	// filhaal ke lie
	
	const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	let validArray = [];


	// alphanumeric check
	let notAlphaNumeric = false;
	let curCode;
	for(i = 0; i < body.username.length; i ++) {
		curCode = body.username.charCodeAt(i);
		if( (curCode < 40) || (curCode > 57 && curCode < 65) || (curCode > 90 && curCode < 97) || (curCode > 122) ) {
			notAlphaNumeric = true;
		}
	}


	let exists = false;
	// length check and finalizing username validity
	if(body.username.length < 4 || notAlphaNumeric) {
		validArray.push(0);
	} else {
		// if username is alpha numeric and length is big enough, proceed to check if username already exists
		try {
			[users, client] = await loadCollection("users");
			let userFound = ( await users.findOne({"username": body.username}));
			//console.log("user found: ");
			//console.log(userFound);
			if(userFound) {
				// case when username already exists
				exists = true;
				// -1 is code for username exists
				validArray.push(-1);
			} else {
				validArray.push(1);
			}
		} catch(e) {
			console.log(e);
			// -2 is code for connection error
			validArray.push(-2);
		} finally {
			client.close();
		}
	}


	if(body.email.match(mailformat)) {
		// email is valid
		validArray.push(1);
	} else {
		validArray.push(0);
	}

	body.password.length < 8 ? validArray.push(0) : validArray.push(1);
	validArray.push(exists);
	//console.log(validArray);

	return validArray;

}


// to authenticate stuff when login
router.post("/login", async (req, res) => {

	// requires username and password in get request body
	const usernameRec = req.body.username;
	[ users, client ] = await loadCollection("users");
	try {
		const user = await users.findOne({username: usernameRec});
		//console.log(user, typeof user);
		if(await argon2.verify(user.password, req.body.password)) {
			// if matched
			//console.log("you are logged in, here, take this jwt");
			authorization = jwt.sign({name: usernameRec }, jwtSecret);
			res.status(200).json({'authorization': `Bearer ${authorization}`});
		} else {
			console.log("Wrong Password!");
			res.status(400).send();
		}
	} catch(e) {
		//console.log("login failed because: ");
		//console.log(e);
		res.status(400).send();
	} finally {
		await client.close();
	}

})


router.post("/create", async (req, res) => {
	// requires username, email and password in post request body
	let validCheckRes = (await userValidate(req.body));
	let fail = "";
	if(validCheckRes[validCheckRes.length-1]) {
		// inform the frontend that username already exists, thats why check failed
		fail = "Username Exists";
	}
	validCheckRes.pop();
	if(JSON.stringify(validCheckRes) == JSON.stringify([1,1,1])) {
		[ users, client ] = await loadCollection("users");
		try {
			await users.insertOne({
				"username": req.body.username,
				"email": req.body.email,
				"password": await argon2.hash(req.body.password),
				"createdAt": new Date(),
				"posts": [],
				"likedPosts": [],
				"likedComments": []
			})
			res.status(201).json({"fail": "no"});
		} catch(e) {
			//console.log("Insert User failed because: ");
			//console.log(e);
			fail = e;
			res.status(400).json({"fail": fail});
		} finally {client.close();}
		
	} else {
		// display user suggestion on correct input at frontend

		fail = "invalid_username_or_password";
		if(validCheckRes[0] == 0) {
			fail = "Please use alphanumeric username";
		} else if(validCheckRes[0] == -1) {
			fail = "Username Already Exists";
		} else if(validCheckRes[0] == -2) {
			fail = "Connection Error";
		} else if(validCheckRes[1] == 0){
			fail = "Invalid email";
		} else {
			fail = "Password too short";
		}
		res.status(400).json({"fail": fail});
	}

})


router.get("/checkAuth", async (req, res) => {
	// to check if a user is authorized by checking jwt by header
	// remove login buttons if returns true
	[ isAuthed, httpCode, username ] = await userAuthed(req);
	//console.log(username);
	res.status(httpCode).json({'isAuthed': isAuthed, 'username': username});
})

router.get("/testpoint", async (req, res) => {
	// just for tests
	res.status(200).json({'yes': 'yes'});
})

router.post("/testpoint", async (req, res) => {
	// just for tests
	console.log(req.body);
	res.status(201).json({'yes': 'posted!'});
})


router.get("/testpointoff", async (req, res) => {
	// just for tests
	res.status(200).json({'no': 'yes'});
})


// tbd: a delete endpoint for users

module.exports = router;
