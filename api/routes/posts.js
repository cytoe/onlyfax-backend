// require('dotenv').config()
const { loadCollection } = require('./loadCollection');
//const config = require('../../config');
const { userAuthed } = require('./authorized');

const express = require('express');
const router = express.Router();

const { MongoClient, ObjectId } = require('mongodb');

// backend for user feed and dashboard
//

const mongoUri = process.env.MONGODB_URI


const watchMyData = async (wait, client, res, username) => {
	const postsCollection = client.db("fax").collection("posts");
	//console.log("collection fetched:");
	const changeStream = postsCollection.watch([]);
	let stillnotreceived = true;
	changeStream.on("change", (next) => {
		//console.log("author of this new post is: ", JSON.stringify(next.fullDocument));
		if(next.operationType == 'delete') {
			//res.status(200);
		}
		else if(username != "any" && next.fullDocument['author'] == username) {
			// it isnt just any username, it is the username and the post is of that username
			res.status(200).send(JSON.stringify(next));
			changeStream.close();
			stillnotreceived = false;
		} else if(username == "any") {
			// get all the changes if feed is needed
			res.status(200).send(JSON.stringify(next));
			changeStream.close();
			stillnotreceived = false;
		}
		//console.log("new object received, closing the change stream");
		//return;
	});
	//console.log("on change assigned!");
	await waitForMessage(wait);
	if(stillnotreceived) {
		//console.log("watchman timed out, closing the change stream");
		changeStream.close();
		res.status(400).json({"timedout": true});
	}
};


const waitForMessage = (wait) => {
	return new Promise((resolve) => {
		setTimeout(() => {
			//console.log("set timout complete!")
			resolve();
		}, wait*1000)
	});
};



router.get("/watchmyposts", async (req, res) => {
	const authorizationArray = await userAuthed(req);
	if(authorizationArray[0]) {
		const client = await new MongoClient(mongoUri);
		try{
			//console.log("connecting the client -");
			await client.connect();
			await watchMyData(120, client, res, authorizationArray[2]);
		} catch(e) {
			//console.log("Oops, an error occured")
			console.log(e);
			res.status(500).send();
		} finally {
			client.close();
		}
	} else {
		res.status(403).send();
	}
});


router.get("/watchposts", async (req, res) => {
	if((await userAuthed(req))[0]) {
		const client = await new MongoClient(mongoUri);
		try{
			//console.log("connecting the client -");
			await client.connect();
			await watchMyData(120, client, res, "any");
		} catch(e) {
			//console.log("Oops, an error occured")
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close();
		}
	} else {
		res.status(403).send();
	}
});


router.get("/feed", async (req, res) => {
	// returns 10 posts, requires page, sortby and authorization header

	// tbd: new plan: return a personalized feed of all users' posts on /:username instead of posts of that particular username

	if((await userAuthed(req))[0]){

		// doing it here because 2 collections are needed here, and 2 clients to do it are unnecessary
		const client = await new MongoClient(mongoUri);
		try{
			await client.connect();
			const db = await client.db("fax");
			const postsCollection = await db.collection("posts");
			const posts = await postsCollection.find();
			let returnArray = [];
			//console.log("headers are :"+ JSON.stringify(req.headers));
			const newposts = parseInt(req.headers['newposts']);
			if(req.headers['sortby'] == "lastadded") {
				const postsSorted = await posts.sort({"_id": -1});
				//console.log("postssorted is ", await postsSorted.toArray());
				let c = 0;
				let ll = (parseInt(req.headers['page'])-1)*9;
				let ul = (parseInt(req.headers['page'])*9);
				// to account for realtime added posts in watchposts function
				ll += newposts;
				ul += newposts;
				//console.log("ll is: ", ll);
				//console.log("ul is: ", ul);
				while((await postsSorted.hasNext()) && c < ul) {
					nextItem = await postsSorted.next();
					//console.log("nextitem being considered is: ", nextItem);
					if(c >= ll) {
						//console.log("next being pushed is:"+ JSON.stringify(nextItem));
						returnArray.push(nextItem);
					}
					c++;
				}
			}
			//console.log(returnArray)
			res.status(200).send(returnArray);
		} catch(e) {
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close()
		}

	} else {
		res.status(403).send({})
	}
})


router.get("/listposts/:username", async (req, res) => {

	// returns all the user's posts given the username

	if((await userAuthed(req))[0]) {

		// doing it here because 2 collections are needed here, and 2 clients to do it are unnecessary
		const client = await new MongoClient(mongoUri);
		try{
			await client.connect();
			const db = await client.db("fax");
			const postsCollection = await db.collection("posts");
			const usersCollection = await db.collection("users");
			const userKiPosts = (await usersCollection.findOne({'username': req.params.username})).posts;
			const posts = await postsCollection.find({_id: {$in: userKiPosts}});
			const postsContent = await posts.toArray();
			res.status(200).send(postsContent);
		} catch(e) {
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close()
		}

	} else {
		res.status(403).send({})
	}
})

router.get("/fetchpost", async (req, res) => {
	// returns the post object given the id
	// requires postid, authorization in headers

	if((await userAuthed(req))[0]){

		const client = await new MongoClient(mongoUri);
		try{
			await client.connect();
			const db = await client.db("fax");
			const postsCollection = await db.collection("posts");
			const post = await postsCollection.findOne({_id: new ObjectId(req.headers['postid'])});
			res.status(200).send(post);
		} catch(e) {
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close()
		}

	} else {
		res.status(403).send({})
	}
})



router.get("/getlikedstuff", async (req, res) => {
	// returns the liked posts array given the username
	// requires username, authorization in headers

	if((await userAuthed(req))[0]){

		const client = await new MongoClient(mongoUri);
		try{
			await client.connect();
			const db = await client.db("fax");
			const usersCollection = await db.collection("users");
			const user = await usersCollection.findOne({username: req.headers['username']});
			const likedPosts = await user.likedPosts;
			const likedComments = await user.likedComments;
			res.status(200).json({
				"likedPosts": likedPosts,
				"likedComments": likedComments
			});
		} catch(e) {
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close()
		}

	} else {
		res.status(403).send({})
	}
})




router.post("/comment", async (req, res) => {
	// take authed token in headers, text field in body, and post object id in body
	// post a comment having fields text, created at
	// take id of created comment and append it to postid's post's comments array
	
	if((await userAuthed(req))[0]) {
		const client = await new MongoClient(mongoUri);
		try {
			await client.connect();
			const db = await client.db("fax");
			const commentsCollection = await db.collection("comments");
			const postsCollection = await db.collection("posts");
			const commentRes = await commentsCollection.insertOne({
				text: req.body.text,
				author: req.body.author,
				postid: req.body.postid,
				likes: 0,
				createdAt: new Date()
			});
			const postKeComments = (await postsCollection.findOne({'_id': new ObjectId(req.body.postid)})).comments
			postKeComments.push(commentRes.insertedId)
			await postsCollection.updateOne({'_id': new ObjectId(req.body.postid)}, {$set: {'comments': postKeComments}});
			res.status(201).send();
		} catch(e) {
			res.status(500).send()
		} finally {
			client.close()
		}
	} else {
		res.status(403).send()
	}

})

router.get("/like", async (req, res) => {
	// likes a post given the post id in header
	// requires postid, username in headers

	if((await userAuthed(req))[0]) {
		const client = await new MongoClient(mongoUri);
		try {

			await client.connect();
			const db = await client.db("fax");
			const postsCollection = await db.collection("posts");
			const usersCollection = await db.collection("users");
			//console.log("successfully connected")
			//console.log("headers user name is :" + req.headers['username'].trim());
			
			// added post id to liked posts for the user
			const likingUser = await usersCollection.findOne({'username': req.headers['username']});
			//console.log("liking user object found")
			const likedArray = await likingUser.likedPosts;
			////console.log("liking user liked posts array found", likedArray)
			likedArray.push(req.headers['postid']);
			//console.log("liked array of user updated: ", likedArray);
			await usersCollection.updateOne({'username': req.headers['username'].trim()}, {$set: {'likedPosts': likedArray}});
			//console.log("user updated with new liked posts");
			
			// incrementing the likes
			const goodPost = await postsCollection.findOne({_id : new ObjectId(req.headers['postid'])});
			//console.log("post to be liked found");
			await postsCollection.updateOne({_id: new ObjectId(req.headers['postid'])}, {$set: {'likes': goodPost.likes+1}})
			//console.log("post updated with new likes");
			res.status(200).send();
		} catch(e) {
			//console.log("liking post failed because: ");
			//console.log(e);
			res.status(500).close();
		} finally {
			client.close();
		}

	} else {
		res.status(403).send();
	}
})




router.get("/comments", async (req, res) => {
	// take authed token in headers, postid field in body
	
	if((await userAuthed(req))[0]) {
		const client = await new MongoClient(mongoUri);
		try {
			await client.connect();
			const db = await client.db("fax");
			const commentsCollection = await db.collection("comments");
			const postsCollection = await db.collection("posts");
			const thatPost = await postsCollection.findOne({'_id': new ObjectId(req.headers['postid'])});
			const comments = await commentsCollection.find({_id: {$in: thatPost.comments}});
			const commentsArray = await comments.toArray();
			res.status(200).send(commentsArray)
		} catch(e) {
			res.status(500).send()
		} finally {
			client.close()
		}
	} else {
		res.status(403).send()
	}

})

router.get("/comment/like", async (req, res) => {
	// likes a comment given the comment id in header
	// requires commentid, username, authorization in headers

	if((await userAuthed(req))[0]) {
		const client = await new MongoClient(mongoUri);
		try {

			await client.connect();
			const db = await client.db("fax");
			const commentsCollection = await db.collection("comments");
			const usersCollection = await db.collection("users");
			
			// added post id to liked posts for the user
			const likingUser = await usersCollection.findOne({'username': req.headers['username']});
			const likedArray = await likingUser.likedComments;
			likedArray.push(req.headers['commentid']);
			await usersCollection.updateOne({'username': req.headers['username'].trim()}, {$set: {'likedComments': likedArray}});
			
			// incrementing the likes
			const goodComment = await commentsCollection.findOne({_id : new ObjectId(req.headers['commentid'])});
			await commentsCollection.updateOne({_id: new ObjectId(req.headers['commentid'])}, {$set: {'likes': goodComment.likes+1}})
			res.status(200).send();
		} catch(e) {
			//console.log("liking comment failed because: ");
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close();
		}

	} else {
		res.status(403).send();
	}
})



router.post("/", async (req, res) => {
	// adds a new post
	// requires text and author name in body

	// tbd: check if authorized user is the same as mentioned in req.body.author
	// 	a solution --> take user name from the jwt instead of req body

	if((await userAuthed(req))[0]) {

		const payload = {
			"text": req.body.text,
			"author": req.body.author,
			"title": req.body.title,
			"createdAt": new Date(),
			"likes": 0,
			"comments": []
		}


		// doing it here because 2 collections are needed here, and 2 clients to do it are unnecessary
		const client = await new MongoClient(mongoUri);
		try{
			// basically add a new post, get the newly created post id, and append it to
			// the previous posts array of the user
			await client.connect();
			const db = await client.db("fax");
			//console.log(db);
			const postsCollection = await db.collection("posts");
			const usersCollection = await db.collection("users");
			const resso = await postsCollection.insertOne(payload);
			//console.log(resso);
			const prevPosts = (await usersCollection.findOne({'username': req.body.author})).posts;
			//console.log(prevPosts);
			prevPosts.push(resso.insertedId);
			//console.log(prevPosts);
			await usersCollection.updateOne({'username': req.body.author}, {$set: {'posts': prevPosts}});
			res.status(201).send()


		} catch(e) {
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close()
		}






	} else {
		res.status(403).send();
	}
})


router.delete("/:id", async (req, res) => {
	// deletes a post given the post id

	// tbd: also check if the id pertains to that particular user in jwt

	if((await userAuthed(req))[0]) {
		try {

			[collection, client] = await loadCollection("posts");
			await collection.deleteOne({_id : new ObjectId(req.params.id)});
			res.status(200).send();
		} catch(e) {
			//console.log("deleting post failed because: ");
			//console.log(e);
			res.status(500).send();
		} finally {
			client.close();
		}

	} else {
		res.status(403).send();
	}
})


module.exports = router;
